#include "common.h"
#include "dbus.h"

#include <string.h>

#ifdef _DBUS_CONSTS_SHARED

#  ifdef DBUS_SERVICES
#    define _(name, value) const char *DBUS_SERVICE_ ## name = #value;
DBUS_SERVICES
#    undef _
#  endif//DBUS_SERVICES

#  ifdef DBUS_INTERFACES
#    define _(name, value) const char *DBUS_INTERFACE_ ## name = #value;
DBUS_INTERFACES
#    undef _
#  endif//DBUS_INTERFACES

#  ifdef DBUS_ERRORS
#    define _(name, value) const char *DBUS_ERROR_ ## name = #value;
DBUS_ERRORS
#    undef _
#  endif//DBUS_ERRORS

#endif//_DBUS_CONSTS_SHARED

DBusConnection* dbus_helper_open(const char* bus_name, const char* service){
  DBusBusType bus_type = DBUS_BUS_SESSION;
  
  if(bus_name){
    if(strcmp(bus_name, ":system:") == 0){
      bus_type = DBUS_BUS_SYSTEM;
      bus_name = NULL;
    }else if(strcmp(bus_name, ":session:") == 0){
      bus_type = DBUS_BUS_SESSION;
      bus_name = NULL;
    }
  }
  
  syslog(LOG_INFO, "Connecting to bus (%s)", bus_name ? bus_name : bus_type == DBUS_BUS_SESSION ? ":session:" : ":system:");
  
  DBusError error;
  dbus_error_init(&error);
  
  DBusConnection* connection = bus_name ? dbus_connection_open(bus_name, &error) : dbus_bus_get(bus_type, &error);
  
  if(dbus_error_is_set(&error)){
    syslog(LOG_ERR, "Connection failed (%s)", error.message);
    dbus_error_free(&error);
    dbus_connection_unref(connection);
    return NULL;
  }
  
  if(NULL == connection){
    return NULL;
  }
  
  if(service){
    syslog(LOG_INFO, "Registering service (%s)", service);
    
    dbus_error_init(&error);
    int ret = dbus_bus_request_name(connection, service, DBUS_NAME_FLAG_REPLACE_EXISTING, &error);
    
    if(dbus_error_is_set(&error)){
      syslog(LOG_ERR, "Registration failed (%s)", error.message);
      dbus_error_free(&error);
      dbus_connection_unref(connection);
      return NULL;
    }
    
    if(DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER != ret){
      dbus_connection_unref(connection);
      return NULL;
    }
  }
  
  return connection;
}

void dbus_helper_close(DBusConnection* connection){
  if(connection){
    syslog(LOG_INFO, "Disconnecting from bus");
    
    dbus_connection_unref(connection);
  }
}
