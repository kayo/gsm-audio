#ifndef __service_h__
#define __service_h__ "service.h"

#include "message.h"
#include "event-dbus.h"

#include "instance.h"

typedef struct service_ service_t;

struct service_ {
  instance_t* instance;
  size_t count;

  /* event loop */
  
  struct event_base* ev_base;
  
  /* life ctl */
  
  struct event ev_term;
  struct event ev_intr;
  
  /* dbus interface */
  event_dbus_t dbus;
  
  message_helper introspect;
};

void service_init(service_t* service);
void service_done(service_t* service);

void service_loop(service_t* service);

#endif//__service_h__
