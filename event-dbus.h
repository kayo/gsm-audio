#ifndef __event_dbus_h__
#define __event_dbus_h__ "event-dbus.h"

typedef struct event_dbus_ event_dbus_t;

typedef void(*event_dbus_cb)(event_dbus_t* edb, void* ud);

struct event_dbus_ {
  const char* bus_name;
  const char* service;
  DBusConnection* conn;
  
  struct event_base* ev_base;
  int ev_pri;
  
  struct event ev_conn;
  struct timeval retry_delay;
  
  struct event ev_data;
  
  event_dbus_cb opened_cb;
  event_dbus_cb closed_cb;
  event_dbus_cb failed_cb;
  void* userdata;
};

void event_dbus_init(event_dbus_t *edb, const char* bus_name, const char* service, struct event_base* ev_base, int ev_pri, event_dbus_cb opened_cb, event_dbus_cb closed_cb, event_dbus_cb failed_cb, void* userdata);

void event_dbus_done(event_dbus_t *event_dbus);

#endif//__event_dbus_h__
