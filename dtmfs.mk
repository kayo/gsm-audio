dtmfs.gen.pulse?=120
dtmfs.gen.pause?=80
dtmfs.gen.level?=15000

dtmfs.det.fragment?=40
dtmfs.det.silence?=640

def+=DTMFS_GEN_PULSE=$(dtmfs.gen.pulse)
def+=DTMFS_GEN_PAUSE=$(dtmfs.gen.pause)
def+=DTMFS_GEN_LEVEL=$(dtmfs.gen.level)

def+=DTMFS_DET_FRAGMENT=$(dtmfs.det.fragment)
def+=DTMFS_DET_SILENCE=$(dtmfs.det.silence)
