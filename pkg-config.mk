ifneq ($(pkg.deps),)
  CFLAGS+=$(shell pkg-config --cflags $(pkg.deps))
  LDFLAGS+=$(shell pkg-config --libs $(pkg.deps))
endif
