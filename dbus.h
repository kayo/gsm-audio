#ifndef __DBUS_H__
#define __DBUS_H__ "dbus.h"

#include <dbus/dbus.h>

#ifdef _DBUS_CONSTS_SHARED

#  ifdef DBUS_SERVICES
#    define _(name, value) extern const char *DBUS_SERVICE_ ## name;
DBUS_SERVICES
#    undef _
#  endif//DBUS_SERVICES

#  ifdef DBUS_INTERFACES
#    define _(name, value) extern const char *DBUS_INTERFACE_ ## name;
DBUS_INTERFACES
#    undef _
#  endif//DBUS_INTERFACES

#  ifdef DBUS_ERRORS
#    define _(name, value) extern const char *DBUS_ERROR_ ## name;
DBUS_ERRORS
#    undef _
#  endif//DBUS_ERRORS

#endif

DBusConnection* dbus_helper_open(const char* bus_name, const char* service);

void dbus_helper_close(DBusConnection* connection);

#endif//__DBUS_H__
