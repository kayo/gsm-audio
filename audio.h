#ifndef __audio_h__
#define __audio_h__ "audio.h"

#include "sound.h"
#include "message.h"

typedef struct audio_ audio_t;

typedef void(*audio_io_cb)(size_t samples, const sample_t* r_sample, sample_t* w_sample, void* ud);

struct audio_ {
  /* Common section */
  
  struct event_base *ev_base;
  
  sample_t captured[AUDIO_IO_SAMPLES];
  sample_t playback[AUDIO_IO_SAMPLES];
  
  /* Modem Audio section */
  
  const char* dev_tty;
  int fd_tty;
  char attach;
  char active;
  
  size_t error_count;
  size_t error_limit;
  
  struct timeval retry_delay;
  struct timeval close_delay;
  
  struct event ev_data; /* data i/o event */
  struct event ev_stop; /* stop i/o event */
  struct event ev_open; /* delayed reopen tty event */
  
  audio_io_cb io_cb;
  void* io_ud;
  
  /* DBus Interface */
  DBusConnection* dbus_conn;
  const char* path;
  
  message_helper get_status;
  message_helper do_attach;
  message_helper do_detach;
  message_helper status_changed;
};

void audio_init(audio_t* audio, const char* path, struct event_base* ev_base, audio_io_cb io_cb, void* io_ud);
void audio_done(audio_t* audio);

void audio_dbus_i11n(audio_t *audio);
void audio_dbus_attach(audio_t* audio, DBusConnection* connection);
void audio_dbus_detach(audio_t* audio, DBusConnection* connection);

#endif//__audio_h__
