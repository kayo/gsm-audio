#ifndef __dtmfs_h__
#define __dtmfs_h__

#include "sound.h"
#include "message.h"
#include "dtmf.h"

typedef struct dtmfs_ dtmfs_t;

struct dtmfs_ {
  /* common */
  int active;
  
  /* dtmf */
  dtmf_gen_t encoder;
  dtmf_det_t decoder;
  
  /* DBus Interface */
  DBusConnection* dbus_conn;
  const char* path;
  
  message_helper send_event;
  message_helper event_received;

  DBusMessage* send_reply;
};

void dtmfs_init(dtmfs_t* dtmfs, const char* path);
void dtmfs_done(dtmfs_t* dtmfs);

void dtmfs_dbus_i11n(dtmfs_t* dtmfs);
void dtmfs_dbus_attach(dtmfs_t* dtmfs, DBusConnection* connection);
void dtmfs_dbus_detach(dtmfs_t* dtmfs, DBusConnection* connection);

void dtmfs_io(dtmfs_t* dtmfs, size_t samples, const sample_t* captured, sample_t* playback);

#endif//__dtmfs_h__
