#ifndef __dtmf_h__
#define __dtmf_h__ "dtmf.h"

#include "sound.h"

#define dtmf_freqs 8
#define dtmf_silence 640

enum dtmf_state_ {
  DTMF_INACT,
  DTMF_PAUSE,
  DTMF_PULSE
};

enum dtmf_error_ {
  DTMF_NONE,
  DTMF_BUSY,
  DTMF_FAIL,
  DTMF_NOEV
};

/* Generator */

typedef struct dtmf_gen_ dtmf_gen_t;

typedef void(*dtmf_gen_cb)(char ok, void* ud);

struct dtmf_gen_ {
  size_t pulse_samples;
  size_t pause_samples;

  sample_module_t sample_amp;
  
  char state;
  char falloff;
  
  float steps[dtmf_freqs];
  
  size_t left_samples;

#ifdef DTMF_USE_MIXER
  sample_mixer_t mixer;
#endif
  
  float step_row;
  float step_col;

  float left_row;
  float left_col;
  
  dtmf_gen_cb cb;
  void* ud;
};

void dtmf_gen_init(dtmf_gen_t* gen, size_t rate/*Hz*/, size_t pulse/*mS*/, size_t pause/*mS*/, sample_module_t amp);
void dtmf_gen_done(dtmf_gen_t* gen);

char dtmf_gen_send(dtmf_gen_t* gen, char evt, dtmf_gen_cb cb, void* ud);
size_t dtmf_gen_data(dtmf_gen_t* gen, sample_t* buffer, size_t samples);

/* Detector */

typedef struct dtmf_det_ dtmf_det_t;

typedef void(*dtmf_det_cb)(char evt, void* ud);

struct dtmf_det_ {
  size_t fragment_samples;
  sample_module_t silence_threshold;
  
  float alpha[dtmf_freqs];
  size_t fragment_offset;
  sample_t* fragment;
  
  char state;
  
  dtmf_det_cb cb;
  void* ud;
};

void dtmf_det_init(dtmf_det_t* det, size_t rate/*Hz*/, size_t fragment/*mS*/, sample_module_t silence/* threshold*/);
void dtmf_det_done(dtmf_det_t* det);

void dtmf_det_recv(dtmf_det_t* det, dtmf_det_cb cb, void* ud);
void dtmf_det_data(dtmf_det_t* det, const sample_t* buffer, size_t samples);

#endif//__dtmf_h__
