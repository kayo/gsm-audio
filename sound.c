#include "sound.h"

#include <stdarg.h>
#include <stdlib.h>

void sample_mixer_init(sample_mixer_t *mixer, size_t count){
  mixer->count = count;
  mixer->pow = sample_max;
  
  for(size_t i = 1; i < mixer->count; i++){
    mixer->pow *= sample_max;
  }
}

/*
  Mixing alg
  
  Lourentz operation:
  
  sum(s)
  -----------------------------
  1 + mul(s) / pow(amp, len(s))
  
  For integers:
  
  sum(s) * pow(amp, len(s))
  -------------------------
  pow(amp, len(s)) + mul(s)
*/

sample_t sample_mixer_step(sample_mixer_t *mixer, sample_t sample, ...){
  va_list rest;
  
  sample_double_t sum = sample;
  sample_double_t mul = sample;
  
  va_start(rest, sample);
  
  for(size_t i = 1; i < mixer->count; i++){
    sample = va_arg(rest, int);
    
    sum += sample;
    mul *= sample;
  }
  
  va_end(rest);
  
  return sum * mixer->pow / (mul + mixer->pow);
}

void sample_mixer_loop(sample_mixer_t *mixer, size_t samples, sample_t *output, const sample_t *input, ...){
  
  const sample_t *buffer[mixer->count];
  buffer[0] = input;
  
  va_list rest;
  va_start(rest, input);
  
  for(size_t i = 1; i < mixer->count; i++){
    buffer[i] = va_arg(rest, const sample_t*);
  }
  
  va_end(rest);
  
  for(size_t s = 0; s < samples; s++){
    sample_double_t sum = buffer[0][s];
    sample_double_t mul = buffer[0][s];
    
    for(size_t i = 1; i < mixer->count; i++){
      sum += buffer[i][s];
      mul *= abs(buffer[i][s]);
    }
    
    output[s] = (int64_t)sum * mixer->pow / (mul + mixer->pow);
  }
}
