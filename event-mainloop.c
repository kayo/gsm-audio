#ifdef PULSE_ENABLE

#include "common.h"
#include "event-mainloop.h"

#ifndef DEBUG_EVENT_MAINLOOP
#  undef LOG
#  define LOG(...)
#endif

typedef struct event_mainloop_ event_mainloop_t;

struct event_mainloop_ {
  struct event_base* ev_base;
  
  /* Priorities */
  int pr_io;
  int pr_time;
  int pr_defer;
};

struct pa_io_event {
  struct event p;
  pa_mainloop_api* a;
  int fd;
  pa_io_event_flags_t ev;
  pa_io_event_cb_t cb;
  void* ud;
  pa_io_event_destroy_cb_t dc;
};

static void io_cb(evutil_socket_t fd, short ev, void *ud){
  pa_io_event* e = ud;
  
  if(e->cb){
    pa_io_event_flags_t pa_ev = PA_IO_EVENT_NULL;
    
    if(e->ev & PA_IO_EVENT_INPUT && ev & EV_READ){
      pa_ev = pa_ev | PA_IO_EVENT_INPUT;
    }
    if(e->ev & PA_IO_EVENT_OUTPUT && ev & EV_WRITE){
      pa_ev = pa_ev | PA_IO_EVENT_OUTPUT;
    }
    
    /*
      FIXME: I don't know how to handle this with libevent2
      
    if(e->ev & PA_IO_EVENT_HANGUP && ){
      pa_ev = pa_io_event_flags_t(pa_ev | PA_IO_EVENT_HANGUP);
    }
    if(e->ev & PA_IO_EVENT_ERROR && ){
      pa_ev = pa_io_event_flags_t(pa_ev | PA_IO_EVENT_ERROR);
    }
    */
    
    if(pa_ev != PA_IO_EVENT_NULL){
      LOG("io_cb(0x%x,fd=%u,ev=%s%s)", e, fd, pa_ev & PA_IO_EVENT_INPUT ? "in" : "", pa_ev & PA_IO_EVENT_OUTPUT ? "out" : "");
      e->cb(e->a, e, e->fd, pa_ev, e->ud);
    }
  }
}

static inline short io_events(pa_io_event_flags_t ev){
  return (ev & PA_IO_EVENT_INPUT ? EV_READ : 0) | (ev & PA_IO_EVENT_OUTPUT ? EV_WRITE : 0) | EV_PERSIST;
}

static pa_io_event* io_new(pa_mainloop_api* a, int fd, pa_io_event_flags_t ev, pa_io_event_cb_t cb, void* ud){
  assert(a);
  assert(a->userdata);
  assert(fd >= 0);
  assert(cb);
  
  event_mainloop_t* m = a->userdata;
  pa_io_event* e = pa_xnew0(pa_io_event, 1);
  
  LOG("io_new(fd=%u,ev=%s%s)->0x%x", fd, ev & PA_IO_EVENT_INPUT ? "in" : "", ev & PA_IO_EVENT_OUTPUT ? "out" : "", e);
  
  e->a = a;
  e->fd = fd;
  e->ev = ev;
  e->cb = cb;
  e->ud = ud;
  
  event_assign(&e->p, m->ev_base, e->fd, io_events(ev), io_cb, e);
  event_priority_set(&e->p, m->pr_io);
  event_add(&e->p, NULL);
  
  return e;
}

static void io_enable(pa_io_event* e, pa_io_event_flags_t ev){
  assert(e);
  
  LOG("io_enable(0x%x,ev=%s%s)", e, ev & PA_IO_EVENT_INPUT ? "in" : "", ev & PA_IO_EVENT_OUTPUT ? "out" : "");
  
  if(e->ev != ev){
    event_del(&e->p);

    if(ev != PA_IO_EVENT_NULL){
      event_mainloop_t* m = e->a->userdata;
      
      e->ev = ev;
      
      event_assign(&e->p, m->ev_base, e->fd, io_events(ev), io_cb, e);
      event_priority_set(&e->p, m->pr_io);
      event_add(&e->p, NULL);
    }
  }
}

static void io_free(pa_io_event* e){
  assert(e);

  LOG("io_free(0x%x)", e);
  
  event_del(&e->p);
  
  if(e->dc){
    e->dc(e->a, e, e->ud);
  }
}

static void io_set_destroy(pa_io_event* e, pa_io_event_destroy_cb_t cb){
  assert(e);
  
  e->dc = cb;
}

/* time */

struct pa_time_event {
  struct event t;
  pa_mainloop_api* a;
  struct timeval tv;
  struct timeval dt;
  pa_time_event_cb_t cb;
  void* ud;
  pa_time_event_destroy_cb_t dc;
};

static void time_cb(evutil_socket_t fd, short ev, void *ud){
  pa_time_event* e = ud;
  
  if(e->cb){
    LOG("time_cb(0x%x,tv=%d:%d), dt=%d:%d", e, e->tv.tv_sec, e->tv.tv_usec, e->dt.tv_sec, e->dt.tv_usec);
    e->cb(e->a, e, &e->tv, e->ud);
  }
}

static inline void timeval_to_delta(struct timeval* dt, const struct timeval* tv){
  struct timeval ct;
  gettimeofday(&ct, NULL);
  
  timersub(tv, &ct, dt);
}

static pa_time_event* time_new(pa_mainloop_api* a, const struct timeval* tv, pa_time_event_cb_t cb, void* ud){
  assert(a);
  assert(a->userdata);
  assert(cb);
  
  event_mainloop_t* m = a->userdata;
  pa_time_event* e = pa_xnew0(pa_time_event, 1);

  e->a = a;
  e->cb = cb;
  e->ud = ud;
  e->tv = *tv;
  
  timeval_to_delta(&e->dt, &e->tv);
  
  LOG("time_new(tv=%d:%d)->0x%x dt=%d:%d", e->tv.tv_sec, e->tv.tv_usec, e, e->dt.tv_sec, e->dt.tv_usec);
  
  event_assign(&e->t, m->ev_base, -1, EV_TIMEOUT, time_cb, e);
  event_priority_set(&e->t, m->pr_time);
  
  if(e->dt.tv_sec > 0 || e->dt.tv_usec > 0){
    event_add(&e->t, &e->dt);
  }
  
  return e;
}

static void time_restart(pa_time_event* e, const struct timeval* tv){
  assert(e);
  
  if(event_pending(&e->t, EV_TIMEOUT, NULL)){
    event_del(&e->t);
  }
  
  e->tv = *tv;
  
  timeval_to_delta(&e->dt, &e->tv);
  
  LOG("time_restart(tv=%d:%d)->0x%x dt=%d:%d", e->tv.tv_sec, e->tv.tv_usec, e, e->dt.tv_sec, e->dt.tv_usec);
  
  if(e->dt.tv_sec > 0 || e->dt.tv_usec > 0){
    event_add(&e->t, &e->dt);
  }
}

static void time_free(pa_time_event* e){
  assert(e);
  
  LOG("time_free(0x%x)", e);
  
  event_del(&e->t);
  
  if(e->dc){
    e->dc(e->a, e, e->ud);
  }
}

static void time_set_destroy(pa_time_event* e, pa_time_event_destroy_cb_t cb){
  assert(e);
  
  e->dc = cb;
}

/* defer */

struct pa_defer_event {
  struct event d;
  pa_mainloop_api* a;
  pa_defer_event_cb_t cb;
  void* ud;
  pa_defer_event_destroy_cb_t dc;
};

static void defer_cb(evutil_socket_t fd, short ev, void *ud){
  pa_defer_event* e = ud;
  
  if(e->cb){
    LOG("defer_cb(0x%x)", e);
    e->cb(e->a, e, e->ud);
  }
}

static pa_defer_event* defer_new(pa_mainloop_api* a, pa_defer_event_cb_t cb, void* ud){
  assert(a);
  assert(a->userdata);
  assert(cb);
  
  event_mainloop_t* m = a->userdata;
  pa_defer_event* e = pa_xnew0(pa_defer_event, 1);
  
  LOG("defer_new()->0x%x", e);
  
  e->a = a;
  e->cb = cb;
  e->ud = ud;
  
  event_assign(&e->d, m->ev_base, -1, EV_PERSIST, defer_cb, e);
  event_priority_set(&e->d, m->pr_defer);
  
  event_add(&e->d, NULL);
  event_active(&e->d, EV_TIMEOUT, 0);
  
  return e;
}

static void defer_enable(pa_defer_event* e, int en){
  assert(e);
  
  LOG("defer_enable(0x%x,en=%u)", e, en);
  
  if(en){
    event_add(&e->d, NULL);
    event_active(&e->d, EV_TIMEOUT, 0);
  }else{
    event_del(&e->d);
  }
}

static void defer_free(pa_defer_event* e){
  assert(e);
  
  LOG("defer_free(0x%x)", e);
  
  event_del(&e->d);
  
  if(e->dc){
    e->dc(e->a, e, e->ud);
  }
}

static void defer_set_destroy(pa_defer_event* e, pa_defer_event_destroy_cb_t cb){
  assert(e);
  
  e->dc = cb;
}

static void quit(pa_mainloop_api* a, int retval){
  /* ignore */
  LOG("quit()");
}

void event_mainloop_api_init(pa_mainloop_api* api, struct event_base* evb, int pr_io, int pr_time, int pr_defer){
  event_mainloop_t* m = api->userdata = pa_xnew0(event_mainloop_t, 1);
  
  m->ev_base = evb;
  m->pr_io = pr_io;
  m->pr_time = pr_time;
  m->pr_defer = pr_defer;
  
  api->io_new = io_new;
  api->io_enable = io_enable;
  api->io_free = io_free;
  api->io_set_destroy = io_set_destroy;
  
  api->time_new = time_new;
  api->time_restart = time_restart;
  api->time_free = time_free;
  api->time_set_destroy = time_set_destroy;
  
  api->defer_new = defer_new;
  api->defer_enable = defer_enable;
  api->defer_free = defer_free;
  api->defer_set_destroy = defer_set_destroy;
  
  api->quit = quit;
}

void event_mainloop_api_done(pa_mainloop_api* api){
  pa_xfree(api->userdata);
}

#endif
