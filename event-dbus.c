#include "common.h"
#include "dbus.h"
#include "event-dbus.h"

static void _event_dbus_data(evutil_socket_t fd, short ev, void *ud);

static void _event_dbus_open(event_dbus_t* edb){
  edb->conn = dbus_helper_open(edb->bus_name, edb->service);

  if(edb->conn){
    event_del(&edb->ev_conn);
    
    int fd;
    
    if(edb->conn && dbus_connection_get_socket(edb->conn, &fd) || dbus_connection_get_unix_fd(edb->conn, &fd)){
      
      event_assign(&edb->ev_data, edb->ev_base, fd, EV_READ | EV_PERSIST, _event_dbus_data, edb);
      event_priority_set(&edb->ev_data, edb->ev_pri);
      
      event_add(&edb->ev_data, NULL);
      
      if(edb->opened_cb){
        edb->opened_cb(edb, edb->userdata);
      }
    }
  }else{
    event_add(&edb->ev_conn, &edb->retry_delay);
  }
}

static void _event_dbus_close(event_dbus_t* edb){
  event_del(&edb->ev_data);
  event_del(&edb->ev_conn);
  
  if(edb->closed_cb){
    edb->closed_cb(edb, edb->userdata);
  }
  
  dbus_connection_unref(edb->conn);
  edb->conn = NULL;
}

static void _event_dbus_data(evutil_socket_t fd, short ev, void *ud){
  event_dbus_t* edb = ud;
  
  if(dbus_connection_read_write(edb->conn, 0)){
    for(DBusDispatchStatus status = dbus_connection_get_dispatch_status(edb->conn);
        status == DBUS_DISPATCH_DATA_REMAINS;
        status = dbus_connection_dispatch(edb->conn)){
      LOG("event dbus i/o operation");
    }
  }else{
    LOG("event dbus lost connection");
    
    event_dbus_cb closed_cb = edb->closed_cb;
    edb->closed_cb = NULL;
    _event_dbus_close(edb);
    edb->closed_cb = closed_cb;
    
    if(edb->failed_cb){
      edb->failed_cb(edb, edb->userdata);
    }
    
    _event_dbus_open(edb);
  }
}

static void _event_dbus_conn(evutil_socket_t fd, short ev, void *ud){
  event_dbus_t* edb = ud;
  
  _event_dbus_open(edb);
}

void event_dbus_init(event_dbus_t *edb, const char* bus_name, const char* service, struct event_base* ev_base, int ev_pri, event_dbus_cb opened_cb, event_dbus_cb closed_cb, event_dbus_cb failed_cb, void* userdata){
  edb->bus_name = bus_name;
  edb->service = service;
  edb->conn = NULL;

  edb->ev_base = ev_base;
  edb->ev_pri = ev_pri;
  
  edb->retry_delay.tv_sec = EVENT_DBUS_RETRY_DELAY;
  edb->retry_delay.tv_usec = 0;
  
  edb->opened_cb = opened_cb;
  edb->closed_cb = closed_cb;
  edb->failed_cb = failed_cb;
  edb->userdata = userdata;
  
  event_assign(&edb->ev_conn, edb->ev_base, -1, EV_TIMEOUT, _event_dbus_conn, edb);
  event_priority_set(&edb->ev_conn, edb->ev_pri);
  
  _event_dbus_open(edb);
}

void event_dbus_done(event_dbus_t *edb){
  _event_dbus_close(edb);
  /*
  edb->opened_cb = NULL;
  edb->closed_cb = NULL;
  edb->failed_cb = NULL;
  edb->userdata = NULL;
  */
}
