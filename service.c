#include "common.h"
#include "service.h"

#include <signal.h>

#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

static int _service_dbus_introspect(DBusConnection *connection, DBusMessage *message, void *ud){
  service_t *service = ud;
  
  DBusMessage *reply = message_helper_return(&service->introspect, message);
  
  message_helper_i11n_iface(DBUS_INTERFACE_INTROSPECTABLE);
  
  message_helper_i11n_method("Introspect");
  message_helper_i11n_method_arg("xml", "s", MESSAGE_OUTPUT);
  message_helper_i11n_method(NULL);
  
  message_helper_i11n_iface(NULL);
  
  for(size_t i = 0; i < service->count; i++){
    message_helper_i11n_node(service->instance[i].name);
  }
  
  const char *i11n = message_helper_i11n();
  
  dbus_message_append_args(reply, DBUS_TYPE_STRING, &i11n, DBUS_TYPE_INVALID);
  message_helper_output(connection, reply);
  
  return 1;
}

static void _service_dbus_bind(event_dbus_t* edb, void* ud){
  service_t* service = ud;
  
  message_helper_attach(&service->introspect, service->dbus.conn);
  
  for(size_t i = 0; i < service->count; i++){
    instance_dbus_attach(&service->instance[i], service->dbus.conn);
  }
}

static void _service_config(service_t* service, const char* file){
  if(!file){
    file = "config.json";
  }
  
  int fd = open(file, O_RDONLY);
  if(fd < 0){
    syslog(LOG_ERR, "Unable to open config (%s)", file);
    return;
  }
  
  struct stat fs;
  fstat(fd, &fs);

  if(fs.st_size == 0){
    close(fd);
    syslog(LOG_ERR, "Config file (%s) is empty", file);
    return;
  }
  
  char *data = mmap(0, fs.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
  
  if(data == MAP_FAILED){
    close(fd);
    syslog(LOG_ERR, "Unable to read file (%s)", file);
    return;
  }
  
  enum json_tokener_error jerr;
  json_object *root = json_tokener_parse_verbose(data, &jerr);
  
  munmap(data, fs.st_size);
  close(fd);
  
  if(jerr != json_tokener_success){
    syslog(LOG_ERR, "Unable to parse config (%s) (%s)", file, json_tokener_error_desc(jerr));
    return;
  }
  
  service->count = json_object_object_length(root);
  
  if(service->count){
    service->instance = calloc(service->count, sizeof(instance_t));
    
    size_t i = 0;
    json_object_object_foreach(root, name, opts){
      instance_init(&service->instance[i], name, service->ev_base, opts);
      
      if(service->dbus.conn){
        instance_dbus_attach(&service->instance[i], service->dbus.conn);
      }
      i++;
    }
  }
}

static void _service_exit(evutil_socket_t fd, short what, void *ud){
  service_t *service = ud;
  
  syslog(LOG_INFO, "Stopping");
  
  /* Done components */
  
  if(service->dbus.conn){
    for(size_t i = 0; i < service->count; i++){
      instance_dbus_detach(&service->instance[i], service->dbus.conn);
    }
    
    message_helper_detach(&service->introspect, service->dbus.conn);
  }
  
  for(size_t i = 0; i < service->count; i++){
    instance_done(&service->instance[i]);
  }
  
  if(service->instance){
    free(service->instance);
    service->instance = NULL;
  }
  
  /* Finalizing DBus */
  
  event_dbus_done(&service->dbus);
  
  /* Deactivate signal handlers */
  
  event_del(&service->ev_term);
  event_del(&service->ev_intr);
}

void service_init(service_t* service){
  syslog(LOG_INFO, "Starting");
  
  /* Init event loop */
  
  service->ev_base = event_base_new();
  
  event_base_priority_init(service->ev_base, 3);
  
  /* Init signal handlers */
  
  event_assign(&service->ev_term, service->ev_base, SIGTERM, EV_SIGNAL | EV_PERSIST, _service_exit, service);
  event_priority_set(&service->ev_term, 2);

  event_assign(&service->ev_intr, service->ev_base, SIGINT, EV_SIGNAL | EV_PERSIST, _service_exit, service);
  event_priority_set(&service->ev_intr, 2);
  
  /* Activate signal handlers */
  
  event_add(&service->ev_term, NULL);
  event_add(&service->ev_intr, NULL);
  
  service->count = 0;
  service->instance = NULL;
  
  /* Initializing DBus */
  
  message_helper_init(&service->introspect,
                      DBUS_SERVICE_GSM,
                      MESSAGE_METHOD,
                      "/",
                      DBUS_INTERFACE_INTROSPECTABLE,
                      "Introspect",
                      _service_dbus_introspect,
                      service);

  event_dbus_init(&service->dbus,
                  getenv("_BUS"), DBUS_SERVICE_GSM,
                  service->ev_base, 0,
                  _service_dbus_bind, NULL, NULL,
                  service);
  
  _service_config(service, getenv("_CONFIG"));
}

void service_done(service_t* service){
  event_base_free(service->ev_base);
  
  syslog(LOG_INFO, "Stopped");
}

void service_loop(service_t* service){
  syslog(LOG_INFO, "Started");
  
  event_base_loop(service->ev_base, 0);
}
