#include "sound.h"
#include "dtmf.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
  sample_t* buffer;
  size_t samples;
  
  char active;
  
  dtmf_gen_t gen;
  dtmf_det_t det;
} test_t;

void test_init(test_t* test){
  test->samples = 160;
  test->buffer = calloc(test->samples, samples_to_bytes(1));
  
  dtmf_gen_init(&test->gen, 8000, 120, 80, sample_max);
  dtmf_det_init(&test->det, 8000, 40, 640);
}

void test_done(test_t* test){
  free(test->buffer);
  
  dtmf_gen_done(&test->gen);
  dtmf_det_done(&test->det);
}

static void _test_send(char ok, void* ud){
  test_t* test = ud;
  
  test->active = 0;
}

void test_send(test_t* test, const char* events, FILE* file){
  for(int i = 0; i < strlen(events); i++){
    if(isspace(events[i])){
      continue;
    }
    
    test->active = 1;
    dtmf_gen_send(&test->gen, events[i], _test_send, test);
    
    for(; test->active; ){
      memset(test->buffer, 0, samples_to_bytes(test->samples));
      size_t samples = dtmf_gen_data(&test->gen, test->buffer, test->samples);
      
      fwrite(test->buffer, samples_to_bytes(1), samples, file);
    }
  }
}

static void _test_recv(char ev, void* ud){
  test_t* test = ud;
  
  printf("%c", ev);
}

void test_recv(test_t* test, FILE* file){
  dtmf_det_recv(&test->det, _test_recv, test);
  
  for(; !feof(file) && !ferror(file); ){
    size_t samples = fread(test->buffer, samples_to_bytes(1), test->samples, file);
    dtmf_det_data(&test->det, test->buffer, samples);
  }
}

int main(int argc, char* argv[]){
  test_t test;
  
  test_init(&test);
  
  if(argc == 3){
    FILE *file = fopen(argv[1], "w");
    test_send(&test, argv[2], file);
    fclose(file);
  }else if(argc == 2){
    FILE *file = fopen(argv[1], "r");
    test_recv(&test, file);
    printf("\n");
    fclose(file);
  }
  
  test_done(&test);
}
