#ifndef __instance_h__
#define __instance_h__ "instance.h"

#include "message.h"

#include "audio.h"
#include "pulse.h"
#include "alsad.h"
#include "dtmfs.h"

typedef struct instance_ instance_t;

struct instance_ {
  char* name;
  
  /* event loop */
  struct event_base* ev_base;
  
  /* sound i/o */
  sample_mixer_t mixer;
  
  audio_t audio;
  
#ifdef PULSE_ENABLE
  pulse_t pulse;
  sample_t pulse_playback[AUDIO_IO_SAMPLES];
#endif

#ifdef ALSA_ENABLE
  alsad_t alsad;
  sample_t alsad_playback[AUDIO_IO_SAMPLES];
#endif
  
  dtmfs_t dtmfs;
  sample_t dtmfs_playback[AUDIO_IO_SAMPLES];

  /* dbus interface */
  DBusConnection* dbus_conn;
  char* path;
  
  message_helper introspect;
};

void instance_init(instance_t* instance, const char* name, struct event_base* ev_base, json_object* opts);
void instance_done(instance_t* instance);

void instance_dbus_attach(instance_t* instance, DBusConnection* connection);
void instance_dbus_detach(instance_t* instance, DBusConnection* connection);

#endif//__instance_h__
