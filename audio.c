#include "common.h"
#include "audio.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

void audio_dbus_i11n(audio_t *audio){
  message_helper_i11n_iface(DBUS_INTERFACE_GSM_AUDIO);
  
  message_helper_i11n_method("Attach");
  message_helper_i11n_method(NULL);

  message_helper_i11n_method("Detach");
  message_helper_i11n_method(NULL);
  
  message_helper_i11n_method("GetStatus");
  message_helper_i11n_method_arg("status", "s", MESSAGE_OUTPUT);
  message_helper_i11n_method(NULL);
  
  message_helper_i11n_signal("StatusChanged");
  message_helper_i11n_signal_arg("status", "s");
  message_helper_i11n_signal(NULL);
  
  message_helper_i11n_iface(NULL);
}

static void _audio_dbus_status_changed(audio_t *audio){
  if(!audio->dbus_conn){
    return;
  }
  
  DBusMessage *event = message_helper_create(&audio->status_changed, NULL);
  const char* status = audio->active ? "online" : audio->attach ? "attach" : "detach";
  
  dbus_message_append_args(event, DBUS_TYPE_STRING, &status, DBUS_TYPE_INVALID);
  message_helper_output(audio->dbus_conn, event);
}

static void audio_tty_init(audio_t* audio);
static void audio_tty_done(audio_t* audio);

static void _audio_attach(audio_t *audio){
  audio->attach = 1;
  _audio_dbus_status_changed(audio);
  audio_tty_init(audio);
}

static void _audio_detach(audio_t *audio){
  audio->attach = 0;
  audio_tty_done(audio);
}

static int _audio_dbus_attach(DBusConnection *connection, DBusMessage *message, void *ud){
  audio_t *audio = ud;
  
  _audio_attach(audio);
  
  DBusMessage *reply = message_helper_return(&audio->do_attach, message);
  message_helper_output(connection, reply);
  
  return 1;
}

static int _audio_dbus_detach(DBusConnection *connection, DBusMessage *message, void *ud){
  audio_t *audio = ud;
  
  _audio_detach(audio);
  
  DBusMessage *reply = message_helper_return(&audio->do_detach, message);
  message_helper_output(connection, reply);
  
  return 1;
}

static int _audio_dbus_get_status(DBusConnection *connection, DBusMessage *message, void *ud){
  audio_t *audio = ud;
  
  DBusMessage *reply = message_helper_return(&audio->get_status, message);

  const char *status = audio->active ? "online" : audio->attach ? "attach" : "detach";
  
  dbus_message_append_args(reply, DBUS_TYPE_STRING, &status, DBUS_TYPE_INVALID);
  message_helper_output(connection, reply);
  
  return 1;
}

void audio_dbus_attach(audio_t* audio, DBusConnection* connection){
  syslog(LOG_INFO, "Attaching Audio interface (%s)", audio->path);
  
  audio->dbus_conn = connection;
  
  message_helper_attach(&audio->do_attach, connection);
  message_helper_attach(&audio->do_detach, connection);
  message_helper_attach(&audio->get_status, connection);
}

void audio_dbus_detach(audio_t* audio, DBusConnection* connection){
  syslog(LOG_INFO, "Detaching Audio interface (%s)", audio->path);
  
  message_helper_detach(&audio->do_attach, connection);
  message_helper_detach(&audio->do_detach, connection);
  message_helper_detach(&audio->get_status, connection);
  
  audio->dbus_conn = NULL;
}

static void _audio_tty_stop(evutil_socket_t fd, short ev, void *ud){
  audio_t *audio = ud;
  
  event_del(&audio->ev_stop);
  
  syslog(LOG_INFO, "Audio stopped");
  audio->active = 0;

  if(audio->io_cb){
    audio->io_cb(0, audio->captured, audio->playback, audio->io_ud);
  }
  
  _audio_dbus_status_changed(audio);
}

static void _audio_tty_data(evutil_socket_t fd, short ev, void *ud){
  audio_t *audio = ud;
  
  ssize_t rlen = read(audio->fd_tty, audio->captured, sizeof(audio->captured));
  
  if(rlen < sizeof(audio->captured)){
    LOG("I/O Error %d %d", rlen, errno);
    if(audio->error_count++ > audio->error_limit){
      audio_tty_done(audio);
      audio_tty_init(audio);
    }
    return;
  }
  
  //LOG("I %d", rlen);
  if(!audio->active){
    syslog(LOG_INFO, "Audio started");
    audio->active = 1;
    
    _audio_dbus_status_changed(audio);
  }
  //event_del(&audio->ev_stop);
  event_add(&audio->ev_stop, &audio->close_delay);
  
  if(audio->io_cb){
    audio->io_cb(samples_from_bytes(rlen), audio->captured, audio->playback, audio->io_ud);
    
    write(audio->fd_tty, audio->playback, rlen);
  }
}

static void _audio_tty_open(evutil_socket_t fd, short ev, void *ud){
  audio_t *audio = ud;
  
  event_del(&audio->ev_open);
  
  if(audio->attach){
    audio_tty_init(audio);
  }
}

static void audio_tty_init(audio_t* audio){
  /* Open TTY audio device */
  
  audio->dev_tty = getenv("_TTYDEV");
  if(!audio->dev_tty){
    audio->dev_tty = "/dev/ttyUSB1";
  }
  
  syslog(LOG_INFO, "Opening Audio (%s)", audio->dev_tty);
  
  audio->fd_tty = open(audio->dev_tty, O_RDWR | O_NOCTTY);
  
  if(audio->fd_tty < 0){
    syslog(LOG_ERR, "Audio failed");
    
    syslog(LOG_INFO, "Audio reopening (during %ds)", audio->retry_delay.tv_sec);
    
    event_add(&audio->ev_open, &audio->retry_delay);
    
    return;
  }
  
  syslog(LOG_INFO, "Audio opened");
  
  /* Switch TTY to raw mode */
  struct termios ti;
  
  memset(&ti, 0, sizeof(ti));
  cfmakeraw(&ti);
  
  tcflush(audio->fd_tty, TCIOFLUSH);
  tcsetattr(audio->fd_tty, TCSANOW, &ti);
  
  audio->error_count = 0;
  
  /* Assign data event listener */
  
  event_assign(&audio->ev_data, audio->ev_base, audio->fd_tty, EV_READ | EV_PERSIST, _audio_tty_data, audio);
  event_priority_set(&audio->ev_data, 1);
  
  event_add(&audio->ev_data, NULL);
}

static void audio_tty_done(audio_t* audio){
  if(audio->active){
    event_active(&audio->ev_stop, EV_TIMEOUT, 0);
  }
  
  event_del(&audio->ev_open);
  
  if(audio->fd_tty > 0){
    event_del(&audio->ev_data);
    
    syslog(LOG_INFO, "Closing Audio %s", audio->dev_tty);
    
    close(audio->fd_tty);
    audio->fd_tty = 0;
  }
}

void audio_init(audio_t* audio, const char* path, struct event_base* ev_base, audio_io_cb io_cb, void* io_ud){
  syslog(LOG_INFO, "Creating Audio");
  
  audio->ev_base = ev_base;
  
  audio->retry_delay.tv_sec = AUDIO_RETRY_DELAY;
  audio->retry_delay.tv_usec = 0;
  
  audio->close_delay.tv_sec = 0;
  audio->close_delay.tv_usec = AUDIO_IO_TIMEOUT * 10;
  
  audio->error_limit = AUDIO_ERROR_LIMIT;
  
  audio->io_cb = io_cb;
  audio->io_ud = io_ud;
  
  audio->dev_tty = NULL;
  audio->fd_tty = 0;
  audio->attach = 0;
  audio->active = 0;
  
  audio->dbus_conn = NULL;
  
  event_assign(&audio->ev_open, audio->ev_base, -1, EV_TIMEOUT, _audio_tty_open, audio);
  event_priority_set(&audio->ev_open, 0);
  
  event_assign(&audio->ev_stop, audio->ev_base, -1, EV_TIMEOUT, _audio_tty_stop, audio);
  event_priority_set(&audio->ev_stop, 0);
  
  /* dbus */

  audio->path = path;

  message_helper_init(&audio->do_attach,
                      DBUS_SERVICE_GSM,
                      MESSAGE_METHOD,
                      audio->path,
                      DBUS_INTERFACE_GSM_AUDIO,
                      "Attach",
                      _audio_dbus_attach,
                      audio);

  message_helper_init(&audio->do_detach,
                      DBUS_SERVICE_GSM,
                      MESSAGE_METHOD,
                      audio->path,
                      DBUS_INTERFACE_GSM_AUDIO,
                      "Detach",
                      _audio_dbus_detach,
                      audio);
  
  message_helper_init(&audio->get_status,
                      DBUS_SERVICE_GSM,
                      MESSAGE_METHOD,
                      audio->path,
                      DBUS_INTERFACE_GSM_AUDIO,
                      "GetStatus",
                      _audio_dbus_get_status,
                      audio);
  
  message_helper_init(&audio->status_changed,
                      DBUS_SERVICE_GSM,
                      MESSAGE_SIGNAL,
                      audio->path,
                      DBUS_INTERFACE_GSM_AUDIO,
                      "StatusChanged",
                      NULL,
                      audio);
  
  const char *attach = getenv("_HOLDTTY");
  if(attach){
    _audio_attach(audio);
  }
}

void audio_done(audio_t* audio){
  syslog(LOG_INFO, "Deleting Audio");
  
  audio_tty_done(audio);
}
