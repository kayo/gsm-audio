#include "dtmf.h"

#include <stdlib.h>
#include <math.h>
#include <string.h>

/*
  |  Hz | 1209 | 1336 | 1477 | 1633 |
  |-----+------+------+------+------|
  | 697 |    1 |    2 |    3 | A    |
  | 770 |    4 |    5 |    6 | B    |
  | 852 |    7 |    8 |    9 | C    |
  | 941 |    * |    0 |    # | D    |
*/

const float freq[] = { /* Frequencies in 2 × PI × KHz */
  2 * M_PI * 0.697, 2 * M_PI * 0.770, 2 * M_PI * 0.852, 2 * M_PI * 0.941, /* low frequency (rows) */
  2 * M_PI * 1.209, 2 * M_PI * 1.336, 2 * M_PI * 1.477, 2 * M_PI * 1.633  /* high frequency (columns) */
};

typedef struct idx_ idx_t;

struct idx_ {
  char row:4;
  char col:4;
};

/* rfc2833 3.10 DTMF Events */
const char* _idx2evt[] = {
  "123A",
  "456B",
  "789C",
  "*0#D",
  "" /*no event [4][4]*/
};

#define idx2evt(row, col) _idx2evt[row][col - 4]

const idx_t _evt2idx[] = {
  {3, 5}, // 0
  
  {0, 4}, // 1
  {0, 5}, // 2
  {0, 6}, // 3
  
  {1, 4}, // 4
  {1, 5}, // 5
  {1, 6}, // 6

  {2, 4}, // 7
  {2, 5}, // 8
  {2, 6}, // 9

  {0, 7}, // A
  {1, 7}, // B
  {2, 7}, // C
  {3, 7}, // D

  {3, 4}, // *
  {3, 6}, // #
  
  {4, 4}
};

static idx_t evt2idx(char evt){
  if(evt >= '0' && evt <= '9'){
    return _evt2idx[evt - '0'];
  }
  if(evt >= 'A' && evt <= 'D'){
    return _evt2idx[evt + 10 - 'A'];
  }
  if(evt >= 'a' && evt <= 'd'){
    return _evt2idx[evt + 10 - 'a'];
  }
  if(evt == '*'){
    return _evt2idx[14];
  }
  if(evt == '#'){
    return _evt2idx[15];
  }
  return _evt2idx[16];
}

/* Generator */

void dtmf_gen_init(dtmf_gen_t* gen, size_t rate/*Hz*/, size_t pulse/*mS*/, size_t pause/*mS*/, sample_module_t amp){
  
  float rate_khz = (float)rate / 1000;
  
  gen->pulse_samples = pulse * rate_khz;
  gen->pause_samples = pause * rate_khz;
  
  gen->sample_amp = amp;
  
  for(size_t i = 0; i < dtmf_freqs; i++){
    gen->steps[i] = freq[i] / rate_khz;
  }

#ifdef DTMF_USE_MIXER
  sample_mixer_init(&gen->mixer, 2);
#endif
  
  gen->state = DTMF_INACT;
  
  gen->cb = NULL;
  gen->ud = NULL;
}

void dtmf_gen_done(dtmf_gen_t* gen){
  if(gen->cb){
    gen->cb(0, gen->ud);
    gen->cb = NULL;
  }
}

char dtmf_gen_send(dtmf_gen_t* gen, char evt, dtmf_gen_cb cb, void* ud){
  if(evt == '\0'){ /* stop */
    gen->state = DTMF_INACT;
    
    if(gen->cb){
      gen->cb(0, gen->ud);
      gen->cb = NULL;
    }
    
    return DTMF_NONE;
  }
  
  if(gen->state == DTMF_INACT){
    idx_t f = evt2idx(evt);
    
    if(f.row < 4){
      gen->state = DTMF_PULSE;
      
      gen->left_samples = 0;
      
      gen->step_row = gen->steps[f.row];
      gen->step_col = gen->steps[f.col];
      
      gen->left_row = 0.0;
      gen->left_col = 0.643501109;
      
      gen->falloff = 0;
      
      gen->cb = cb;
      gen->ud = ud;
      
      return DTMF_NONE;
    }
    
    return DTMF_NOEV;
  }
  
  return DTMF_BUSY;
}

size_t dtmf_gen_data(dtmf_gen_t* gen, sample_t* buffer, size_t samples){
  if(gen->state == DTMF_PULSE){
    size_t end = gen->left_samples + samples;
    size_t left_buffer = 0;
    
    for(; gen->left_samples < end; gen->left_samples++, left_buffer++){
#ifdef DTMF_USE_MIXER
      buffer[left_buffer] = (0.8 * cos(gen->left_row) - cos(gen->left_col)) / 1.8 * gen->sample_amp;
#else
      buffer[left_buffer] = sample_mixer_step(&gen->mixer, 0.8 * cos(gen->left_row) * gen->sample_amp, cos(gen->left_col) * gen->sample_amp);
#endif
      
      if(gen->left_samples == gen->pulse_samples){
        gen->falloff = 1;
      }
      
      if(gen->falloff && abs(buffer[left_buffer]) < dtmf_silence){
        gen->state = DTMF_PAUSE;
        gen->left_samples = 0;
        break;
      }
      
      gen->left_row += gen->step_row;
      gen->left_col += gen->step_col;
    }
    
    return left_buffer;
  }
  
  if(gen->state == DTMF_PAUSE){
    size_t right = gen->pause_samples - gen->left_samples;

    if(samples > right){
      samples = right;
    }
    
    memset(buffer, 0, samples_to_bytes(samples));
    gen->left_samples += samples;
    
    if(gen->left_samples == gen->pause_samples){
      gen->state = DTMF_INACT;
      gen->left_samples = 0;
      
      if(gen->cb){
        gen->cb(1, gen->ud);
        gen->cb = NULL;
      }
    }
    
    return samples;
  }

  return 0;
}

/* Detector */

void dtmf_det_init(dtmf_det_t* det, size_t rate/*Hz*/, size_t fragment/*mS*/, sample_module_t silence/* threshold*/){
  
  det->fragment_samples = fragment * rate / 1000;
  det->silence_threshold = silence;
  
  for(size_t i = 0; i < dtmf_freqs; i++){
    det->alpha[i] = cos((M_PI + freq[i] * fragment) / det->fragment_samples) * 2;
  }
  
  det->cb = NULL;
  det->ud = NULL;
  
  det->state = DTMF_INACT;
  det->fragment_offset = 0;
  
  det->fragment = calloc(det->fragment_samples, sizeof(sample_t));
}

void dtmf_det_done(dtmf_det_t* det){
  free(det->fragment);
  
  det->cb = NULL;
}

void dtmf_det_recv(dtmf_det_t* det, dtmf_det_cb cb, void* ud){
  det->cb = cb;
  det->ud = ud;
  
  det->state = DTMF_INACT;
  det->fragment_offset = 0;
}

static void _dtmf_det_goertzel(float M[dtmf_freqs], float alpha[dtmf_freqs], const sample_t* buffer, size_t start, size_t count){
  
  size_t end = start + count;
  float S0, S1, S2;
  
  for(size_t i = 0; i < dtmf_freqs; i++){
    float a = alpha[i];
    
    S1 = 0;
    S2 = 0;
    
    for(size_t n = start; n < end; ){
      S0 = a * S1 - S2 + buffer[n++];
      S2 = S1;
      S1 = S0;
    }
    
    M[i] = S1 * S1 + S2 * S2 - S1 * S2 * a;
  }
}

static char _dtmf_det_select(float amps[dtmf_freqs], char start, char end){
  float M = 0;
  float m = 0;
  char I;
  
  for(size_t i = start; i < end; i++){
    if(amps[i] > M){
      m = M;
      M = amps[i];
      I = i;
    }else if(amps[i] > m){
      m = amps[i];
    }
  }
  
  if(M > m * 10){
    return I;
  }
  
  return -1;
}

static char _dtmf_det_fragment(float alpha[dtmf_freqs], const sample_t* buffer, size_t start, size_t count){
  
  float amps[dtmf_freqs];
  _dtmf_det_goertzel(amps, alpha, buffer, start, count);
  
  char row = _dtmf_det_select(amps, 0, 4);
  char col = _dtmf_det_select(amps, 4, 8);
  
  if(row >= 0 && row < 4 && col >= 4 && col < 8){
    return idx2evt(row, col);
  }
  
  return idx2evt(4, 4);
}

static sample_t _dtmf_det_average(const sample_t* buffer, size_t start, size_t count){
  
  size_t end = start + count;
  sample_double_t avg = 0;
  
  for(size_t i = start; i < end; i++){
    avg += abs(buffer[i]);
  }
  
  return avg / count;
}

static void _dtmf_det_step(dtmf_det_t* det){
  if(_dtmf_det_average(det->fragment, 0, det->fragment_samples) > det->silence_threshold){
    if(det->state != DTMF_PULSE){ /* detect once per pulse */
      char evt = _dtmf_det_fragment(det->alpha, det->fragment, 0, det->fragment_samples);
      if(evt == '\0'){ /* no useful signal */
        det->state = DTMF_PAUSE;
      }else{ /* detected event */
        det->cb(evt, det->ud);
        det->state = DTMF_PULSE;
      }
    }
  }else{
    det->state = DTMF_PAUSE;
  }
}

void dtmf_det_data(dtmf_det_t* det, const sample_t* buffer, size_t samples){
  if(det->cb){
    for(size_t left_samples = 0, step_samples = det->fragment_samples;
        left_samples < samples; left_samples += step_samples){
      
      if(step_samples > samples - left_samples){
        step_samples = samples - left_samples;
      }
      
      if(step_samples > det->fragment_samples - det->fragment_offset){
        step_samples = det->fragment_samples - det->fragment_offset;
      }
      
      memcpy(det->fragment + det->fragment_offset, buffer, samples_to_bytes(step_samples));
      det->fragment_offset += step_samples;
      
      if(det->fragment_offset == det->fragment_samples){
        _dtmf_det_step(det);
        
        det->fragment_offset = 0;
        
        step_samples = det->fragment_samples;
      }
    }
  }
}
