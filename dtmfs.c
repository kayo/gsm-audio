#include "common.h"
#include "dtmfs.h"

static void _dtmfs_dbus_event_received(dtmfs_t *dtmfs, const char* name){
  if(!dtmfs->dbus_conn){
    return;
  }
  
  DBusMessage *event = message_helper_create(&dtmfs->event_received, NULL);
  
  dbus_message_append_args(event, DBUS_TYPE_STRING, &name, DBUS_TYPE_INVALID);
  message_helper_output(dtmfs->dbus_conn, event);
}

static void _dtmfs_recv_notify(char event, void* ud){
  dtmfs_t *dtmfs = ud;
  
  char name[2];
  name[0] = event;
  name[1] = '\0';
  
  LOG("Received DTMF event (%s)", name);
  
  _dtmfs_dbus_event_received(dtmfs, name);
}

void dtmfs_io(dtmfs_t* dtmfs, size_t samples, const sample_t* captured, sample_t* playback){
  int active = samples > 0 ? 1 : 0;
  
  if(active != dtmfs->active){ /* toggle activity */
    if(active){ /* start */
      dtmf_det_recv(&dtmfs->decoder, _dtmfs_recv_notify, dtmfs);
    }else{ /* stop */
      dtmf_gen_send(&dtmfs->encoder, 0, NULL, NULL);
      dtmf_det_recv(&dtmfs->decoder, NULL, NULL);
    }
    
    dtmfs->active = active;
  }
  
  if(dtmfs->active){
    dtmf_det_data(&dtmfs->decoder, captured, samples);
    dtmf_gen_data(&dtmfs->encoder, playback, samples);
  }
}

void dtmfs_dbus_i11n(dtmfs_t* dtmfs){
  message_helper_i11n_iface(DBUS_INTERFACE_GSM_EVENT);
  
  message_helper_i11n_method("Send");
  message_helper_i11n_method_arg("event", "s", MESSAGE_INPUT);
  message_helper_i11n_method_arg("event", "b", MESSAGE_OUTPUT);
  message_helper_i11n_method(NULL);
  
  message_helper_i11n_signal("Received");
  message_helper_i11n_signal_arg("event", "s");
  message_helper_i11n_signal(NULL);
  
  message_helper_i11n_iface(NULL);
}

static void _dtmfs_send_reply(char ok, void* ud){
  dtmfs_t *dtmfs = ud;
  
  if(dtmfs->dbus_conn && dtmfs->send_reply){
    dbus_bool_t st = ok ? TRUE : FALSE;
    
    dbus_message_append_args(dtmfs->send_reply, DBUS_TYPE_BOOLEAN, &st, DBUS_TYPE_INVALID);
    
    message_helper_output(dtmfs->dbus_conn, dtmfs->send_reply);
    dtmfs->send_reply = NULL;
  }
}

static inline void _dtmfs_kill_reply(dtmfs_t *dtmfs){
  _dtmfs_send_reply(0, dtmfs);
}

static int _dtmfs_dbus_send_event(DBusConnection *connection, DBusMessage *message, void *ud){
  dtmfs_t *dtmfs = ud;
  
  if(!dtmfs->active){
    message_helper_except(connection, message, DBUS_ERROR_EVENT_OFFLINE, "Audio interface offline");
    return 1;
  }
  
  const char* event = NULL;
  
  dbus_message_get_args(message, NULL, DBUS_TYPE_STRING, &event, DBUS_TYPE_INVALID);
  
  if(!event){
    message_helper_except(connection, message, DBUS_ERROR_EVENT_NO, "Event argument required");
    return 1;
  }
  
  char ret = dtmf_gen_send(&dtmfs->encoder, event[0], _dtmfs_send_reply, dtmfs);
  
  if(ret == DTMF_BUSY){
    message_helper_except(connection, message, DBUS_ERROR_EVENT_BUSY, "Generator is busy");
    return 1;
  }
  
  if(ret == DTMF_NOEV){
    message_helper_except(connection, message, DBUS_ERROR_EVENT_INVALID, "Invalid event");
    return 1;
  }
  
  LOG("Send DTMF event (%s)", event);
  
  _dtmfs_kill_reply(dtmfs);
  
  dtmfs->send_reply = message_helper_return(&dtmfs->send_event, message);
  
  return 1;
}

void dtmfs_init(dtmfs_t* dtmfs, const char* path){
  syslog(LOG_INFO, "Creating DTMF codec");
  
  dtmfs->active = 0;
  dtmfs->send_reply = NULL;
  
  dtmfs->path = path;
  
  message_helper_init(&dtmfs->send_event,
                      DBUS_SERVICE_GSM,
                      MESSAGE_METHOD,
                      dtmfs->path,
                      DBUS_INTERFACE_GSM_EVENT,
                      "Send",
                      _dtmfs_dbus_send_event,
                      dtmfs);
  
  message_helper_init(&dtmfs->event_received,
                      DBUS_SERVICE_GSM,
                      MESSAGE_SIGNAL,
                      dtmfs->path,
                      DBUS_INTERFACE_GSM_EVENT,
                      "Received",
                      NULL,
                      dtmfs);
  
  dtmf_gen_init(&dtmfs->encoder, AUDIO_RATE, DTMFS_GEN_PULSE, DTMFS_GEN_PAUSE, DTMFS_GEN_LEVEL);
  dtmf_det_init(&dtmfs->decoder, AUDIO_RATE, DTMFS_DET_FRAGMENT, DTMFS_DET_SILENCE);
}

void dtmfs_done(dtmfs_t* dtmfs){
  syslog(LOG_INFO, "Deleting DTMF codec");
  
  _dtmfs_kill_reply(dtmfs);
  
  dtmf_gen_done(&dtmfs->encoder);
  dtmf_det_done(&dtmfs->decoder);
}

void dtmfs_dbus_attach(dtmfs_t* dtmfs, DBusConnection* connection){
  syslog(LOG_INFO, "Attaching DTMF codec interface (%s)", dtmfs->path);
  
  dtmfs->dbus_conn = connection;
  
  message_helper_attach(&dtmfs->send_event, connection);
}

void dtmfs_dbus_detach(dtmfs_t* dtmfs, DBusConnection* connection){
  syslog(LOG_INFO, "Detaching DTMF codec interface (%s)", dtmfs->path);
  
  message_helper_detach(&dtmfs->send_event, connection);
  
  dtmfs->dbus_conn = NULL;
}
