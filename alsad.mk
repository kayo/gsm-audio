ifeq ($(alsa),on)

def+=ALSA_ENABLE=1

pkg.deps+=alsa

alsa.latency?=500000

def+=ALSA_LATENCY=$(alsa.latency)

endif
