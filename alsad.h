#ifdef ALSA_ENABLE

#ifndef __alsad_h__
#define __alsad_h__ "alsad.h"

typedef struct alsad_ alsad_t;

typedef struct alsad_stream_ alsad_stream_t;

#include "sound.h"

#include <asoundlib.h>

struct alsad_stream_ {
  snd_pcm_stream_t dir; /* stream direction */
  char* dev; /* associated device name */
  uint32_t lat; /* interaction latency */
  snd_pcm_t *pcm; /* target pcm */
};

struct alsad_ {
  alsad_stream_t capture;
  alsad_stream_t playback;
  
  char active;
};

void alsad_init(alsad_t* alsad, struct event_base* ev_base, json_object* opts);
void alsad_done(alsad_t* alsad);

void alsad_io(alsad_t* alsad, size_t samples, const sample_t* captured, sample_t* playback);

#endif//__alsad_h__

#endif//ALSA_ENABLE
