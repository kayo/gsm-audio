#include "common.h"
#include "instance.h"

#include <string.h>

void _instance_audio_io(size_t samples, const sample_t* captured, sample_t* playback, void* ud){
  instance_t* instance = ud;
  
#ifdef PULSE_ENABLE
  memset(instance->pulse_playback, 0, sizeof(instance->pulse_playback));
  pulse_io(&instance->pulse, samples, captured, instance->pulse_playback);
#endif

#ifdef ALSA_ENABLE
  memset(instance->alsad_playback, 0, sizeof(instance->alsad_playback));
  alsad_io(&instance->alsad, samples, captured, instance->alsad_playback);
#endif
  
  memset(instance->dtmfs_playback, 0, sizeof(instance->dtmfs_playback));
  dtmfs_io(&instance->dtmfs, samples, captured, instance->dtmfs_playback);
  
#ifdef PULSE_ENABLE
  sample_mixer_loop(&instance->mixer, samples, playback, instance->pulse_playback, instance->dtmfs_playback);
#endif

#ifdef ALSA_ENABLE
  sample_mixer_loop(&instance->mixer, samples, playback, instance->alsad_playback, instance->dtmfs_playback);
#endif
}

static int _instance_dbus_introspect(DBusConnection *connection, DBusMessage *message, void *ud){
  instance_t *instance = ud;
  
  DBusMessage *reply = message_helper_return(&instance->introspect, message);
  
  message_helper_i11n_iface(DBUS_INTERFACE_INTROSPECTABLE);
  
  message_helper_i11n_method("Introspect");
  message_helper_i11n_method_arg("xml", "s", MESSAGE_OUTPUT);
  message_helper_i11n_method(NULL);
  
  message_helper_i11n_iface(NULL);
  
  audio_dbus_i11n(&instance->audio);
  dtmfs_dbus_i11n(&instance->dtmfs);
  
  const char *i11n = message_helper_i11n();
  
  dbus_message_append_args(reply, DBUS_TYPE_STRING, &i11n, DBUS_TYPE_INVALID);
  message_helper_output(connection, reply);
  
  return 1;
}

void instance_dbus_attach(instance_t* instance, DBusConnection* connection){
  instance->dbus_conn = connection;
  
  syslog(LOG_INFO, "Attaching Instance (%s)", instance->path);
  message_helper_attach(&instance->introspect, instance->dbus_conn);
  
  audio_dbus_attach(&instance->audio, instance->dbus_conn);
  dtmfs_dbus_attach(&instance->dtmfs, instance->dbus_conn);
}

void instance_dbus_detach(instance_t* instance, DBusConnection* connection){
  audio_dbus_detach(&instance->audio, instance->dbus_conn);
  dtmfs_dbus_detach(&instance->dtmfs, instance->dbus_conn);
  
  syslog(LOG_INFO, "Detaching Instance (%s)", instance->path);
  message_helper_detach(&instance->introspect, instance->dbus_conn);
  
  instance->dbus_conn = NULL;
}

void instance_init(instance_t* instance, const char* name, struct event_base* ev_base, json_object* opts){
  instance->name = strdup(name);
  instance->path = message_helper_mkpath(NULL, instance->name);
  
  syslog(LOG_INFO, "Creating instance (%s)", instance->name);
  
  instance->ev_base = ev_base;
  
  /* Init components */
  
  sample_mixer_init(&instance->mixer, 2);
  
  audio_init(&instance->audio, instance->path, instance->ev_base, _instance_audio_io, instance);
#ifdef PULSE_ENABLE
  pulse_init(&instance->pulse, instance->ev_base);
#endif
#ifdef ALSA_ENABLE
  alsad_init(&instance->alsad, instance->ev_base, json_object_object_get(opts, "alsa"));
#endif
  dtmfs_init(&instance->dtmfs, instance->path);
  
  /* Init DBus */
  
  message_helper_init(&instance->introspect,
                      DBUS_SERVICE_GSM,
                      MESSAGE_METHOD,
                      instance->path,
                      DBUS_INTERFACE_INTROSPECTABLE,
                      "Introspect",
                      _instance_dbus_introspect,
                      instance);
}

void instance_done(instance_t* instance){
  dtmfs_done(&instance->dtmfs);
#ifdef PULSE_ENABLE
  pulse_done(&instance->pulse);
#endif
#ifdef ALSA_ENABLE
  alsad_done(&instance->alsad);
#endif
  audio_done(&instance->audio);
  
  syslog(LOG_INFO, "Deleting instance (%s)", instance->name);
  
  if(instance->path){
    free(instance->path);
    instance->path = NULL;
  }

  if(instance->name){
    free(instance->name);
    instance->name = NULL;
  }
}

