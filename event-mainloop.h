#ifdef PULSE_ENABLE

#ifndef __event_mainloop_h__
#define __event_mainloop_h__ "event-mainloop.h"

#include <pulse/pulseaudio.h>

void event_mainloop_api_init(pa_mainloop_api* api, struct event_base* evb, int pr_io, int pr_time, int pr_defer);

void event_mainloop_api_done(pa_mainloop_api* api);

#endif//__event_mainloop_h__

#endif
