#ifndef __common_h__
#define __common_h__ "common.h"

#include <stdint.h>
#include <stdlib.h>

#include <assert.h>

#include <syslog.h>

#include <event2/event.h>
#include <event2/event_struct.h>

#include <json.h>

#include "dbus.h"

#ifdef DEBUG
#  define LOG(fmt, arg...) syslog(LOG_DEBUG, "%s:%d\t" fmt, __FILE__, __LINE__, ##arg)
#else
#  define LOG(fmt, arg...)
#endif

#endif//__common_h__
