#ifdef PULSE_ENABLE

#include "common.h"
#include "pulse.h"
#include "event-mainloop.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

static void _pulse_source_init(pulse_t* pulse){
  pulse->fd_source = open(pulse->pipe_source, O_WRONLY | O_APPEND | O_NONBLOCK);
  
  if(pulse->fd_source > 0){
    syslog(LOG_INFO, "Source pipe opened");
  }else{
    syslog(LOG_ERR, "Source pipe failed");
  }
}

static void _pulse_source_done(pulse_t* pulse){
  if(pulse->fd_source > 0){
    syslog(LOG_INFO, "Closing source pipe");
    
    close(pulse->fd_source);
    pulse->fd_source = 0;
  }
}

static void _pulse_sink_data(evutil_socket_t fd, short ev, void *ud){
  pulse_t *pulse = ud;
  /*
  ssize_t rlen;
  
  for(; (rlen = read(pulse->fd_sink, pulse->data_buf, sizeof(pulse->data_buf))) == sizeof(pulse->data_buf); ){
    LOG("pa data %d", rlen);
  }
  
  pulse->data_len = rlen > 0 ? rlen : rlen < 0 ? 0 : sizeof(pulse->data_buf);
  LOG("pa len %d", pulse->data_len);
  */
  
  pulse->data_len = read(pulse->fd_sink, pulse->data_buf, sizeof(pulse->data_buf));
}

static void _pulse_sink_init(pulse_t* pulse){
  pulse->fd_sink = open(pulse->pipe_sink, O_RDONLY | O_NDELAY | O_NONBLOCK);
  
  if(pulse->fd_sink > 0){
    syslog(LOG_INFO, "Sink pipe opened");
    
    //event_assign(&pulse->ev_data, pulse->ev_base, pulse->fd_sink, EV_READ | EV_PERSIST, _pulse_sink_data, pulse);
    event_assign(&pulse->ev_data, pulse->ev_base, -1, EV_TIMEOUT | EV_PERSIST, _pulse_sink_data, pulse);
    event_priority_set(&pulse->ev_data, 1);
    
    event_add(&pulse->ev_data, &pulse->io_delay);
  }else{
    syslog(LOG_ERR, "Sink pipe failed");
  }
}

static void _pulse_sink_done(pulse_t* pulse){
  if(pulse->fd_sink > 0){
    syslog(LOG_INFO, "Closing sink pipe");
    
    event_del(&pulse->ev_data);
    
    close(pulse->fd_sink);
    pulse->fd_sink = 0;
  }
}

void pulse_io(pulse_t* pulse, size_t samples, const sample_t* captured, sample_t* playback){
  char active = samples > 0 ? 1 : 0;
  
  size_t size = samples_to_bytes(samples);
  
  if(active != pulse->active){ /* toggle activity */
    if(active){ /* start */
      /* we need to seek the stream to reduce latency */
      //for(ssize_t rlen; (rlen = read(pulse->fd_sink, pulse->data_buf, size)) == size; );
      //_pulse_source_init(pulse);
      //_pulse_sink_init(pulse);
    }else{ /* stop */
      /* we need to drop all unplayed data */
      //for(ssize_t rlen; (rlen = read(pulse->fd_sink, pulse->data_buf, size)) > 0; );
      //_pulse_source_done(pulse);
      //_pulse_sink_done(pulse);
    }
    
    pulse->active = active;
  }
  
  if(pulse->active){
    if(pulse->fd_source > 0){
      write(pulse->fd_source, captured, size);
    }
    
    if(pulse->fd_sink > 0 && pulse->data_len > 0){
      //read(pulse->fd_sink, playback, size);
      //ssize_t rlen = read(pulse->fd_sink, pulse->data_buf, size);
      size_t rlen = pulse->data_len;
      pulse->data_len = 0;
      if(rlen < size){
        size_t elen = size - rlen;
        memcpy((char*)playback, (char*)pulse->data_buf + rlen, elen);
        memcpy((char*)playback + elen, pulse->data_buf, rlen);
      }else{
        memcpy(playback, pulse->data_buf, size);
      }
    }
  }
}

static void _pulse_source_added(pa_context *c, uint32_t idx, void *ud){
  pulse_t* pulse = ud;

  if(idx > 0){
    syslog(LOG_INFO, "Opening source pipe (%s) for writing", pulse->pipe_source);
    
    pulse->idx_source = idx;
    _pulse_source_init(pulse);
  }else{
    syslog(LOG_ERR, "Creating source device failed");
  }
}

static void _pulse_sink_added(pa_context *c, uint32_t idx, void *ud){
  pulse_t* pulse = ud;

  if(idx > 0){
    syslog(LOG_INFO, "Opening sink pipe (%s) for reading", pulse->pipe_sink);
    
    pulse->idx_sink = idx;
    _pulse_sink_init(pulse);
  }else{
    syslog(LOG_ERR, "Creating sink device failed");
  }
}

static void pulse_dev_init(pulse_t* pulse){
  char str[1024];
  
  pulse->pipe_source = getenv("_SOURCE");
  if(!pulse->pipe_source){
    pulse->pipe_source = "/var/run/" PROJECT "/source.pipe";
  }
  
  snprintf(str, sizeof(str), "source_name='" PULSE_SOURCE_NAME "' source_properties='" PA_PROP_DEVICE_STRING "=\"" PULSE_SOURCE_NAME "\" " PA_PROP_DEVICE_DESCRIPTION "=\"" PULSE_SOURCE_DESC "\"' file='%s' " PULSE_SOURCE_OPTS, pulse->pipe_source);

  if(pulse->modld){
    syslog(LOG_INFO, "Creating source device");
    
    LOG("load-module module-pipe-source %s", str);
    
    pa_context_load_module(pulse->pa_ctx, "module-pipe-source", str, _pulse_source_added, pulse);
  }else{
    syslog(LOG_NOTICE, "You need to add source manually before start service by using (load-module module-pipe-source %s)", str);
    
    _pulse_source_added(pulse->pa_ctx, 1, pulse);
  }
  
  pulse->pipe_sink = getenv("_SINK");
  if(!pulse->pipe_sink){
    pulse->pipe_sink = "/var/run/" PROJECT "/sink.pipe";
  }
    
  snprintf(str, sizeof(str), "sink_name='" PULSE_SINK_NAME "' sink_properties='" PA_PROP_DEVICE_STRING "=\"" PULSE_SINK_NAME "\" " PA_PROP_DEVICE_DESCRIPTION "=\"" PULSE_SINK_DESC "\"' file='%s' " PULSE_SINK_OPTS, pulse->pipe_sink);

  if(pulse->modld){
    syslog(LOG_INFO, "Creating sink device");
    
    LOG("load-module module-pipe-sink %s", str);
    
    pa_context_load_module(pulse->pa_ctx, "module-pipe-sink", str, _pulse_sink_added, pulse);
  }else{
    syslog(LOG_NOTICE, "You need to add sink manually before start service by using (load-module module-pipe-sink %s)", str);
    
    _pulse_sink_added(pulse->pa_ctx, 1, pulse);
  }
}

static void _pulse_final(pulse_t* pulse){
  if(pulse->idx_source || pulse->idx_sink){
    return;
  }
  
  if(pulse->pa_state == PA_CONTEXT_READY){
    syslog(LOG_INFO, "Disconnecting Pulseaudio Context");
    
    pa_context_disconnect(pulse->pa_ctx);
  }
}

static void _pulse_source_removed(pa_context *c, int success, void *ud){
  pulse_t* pulse = ud;
  
  pulse->idx_source = 0;
  
  _pulse_final(pulse);
}

static void _pulse_sink_removed(pa_context *c, int success, void *ud){
  pulse_t* pulse = ud;
  
  pulse->idx_sink = 0;
  
  _pulse_final(pulse);
}

static void pulse_dev_done(pulse_t* pulse){
  if(pulse->idx_source){
    _pulse_source_done(pulse);
    
    if(pulse->pa_state == PA_CONTEXT_READY && pulse->modld){
      syslog(LOG_INFO, "Deleting source dev");
      
      pa_context_unload_module(pulse->pa_ctx, pulse->idx_source, _pulse_source_removed, pulse);
    }else{
      pulse->idx_source = 0;
    }
  }
  
  if(pulse->idx_sink){
    _pulse_sink_done(pulse);
    
    if(pulse->pa_state == PA_CONTEXT_READY && pulse->modld){
      syslog(LOG_INFO, "Deleting sink dev");
      
      pa_context_unload_module(pulse->pa_ctx, pulse->idx_sink, _pulse_sink_removed, pulse);
    }else{
      pulse->idx_sink = 0;
    }
  }

  _pulse_final(pulse);
}

static void _pulse_ctx_clean(pulse_t* pulse){
  if(pulse->pa_ctx){
    syslog(LOG_INFO, "Deleting Pulseaudio Context");
    
    pa_context_unref(pulse->pa_ctx);
    pulse->pa_ctx = NULL; 
  }
  
  event_mainloop_api_done(&pulse->pa_mainloop);
}

static void _pulse_ctx_state(pa_context *c, void *ud){
  pulse_t* pulse = ud;
  
  pulse->pa_state = pa_context_get_state(pulse->pa_ctx);
  
  switch(pulse->pa_state){
  case PA_CONTEXT_READY:
    syslog(LOG_INFO, "Pulseaudio Context ready");
    
    pulse_dev_init(pulse);
    break;
    
  case PA_CONTEXT_FAILED:
    if(!event_pending(&pulse->ev_conn, EV_TIMEOUT, NULL)){
      syslog(LOG_INFO, "Pulseaudio Context failed (%s)", pa_strerror(pa_context_errno(pulse->pa_ctx)));
      
      pulse_dev_done(pulse);
      _pulse_ctx_clean(pulse);
      
      syslog(LOG_INFO, "Pulseaudio Context reconnecting (during %ds)", pulse->retry_delay.tv_sec);
      
      event_add(&pulse->ev_conn, &pulse->retry_delay);
    }
    break;
    
  case PA_CONTEXT_TERMINATED:
    _pulse_ctx_clean(pulse);
    break;
  }
}

static void pulse_ctx_init(pulse_t* pulse){
  syslog(LOG_INFO, "Creating Pulseaudio Context");
  
  /* Initialize Pulseaudio mainloop API */
  
  event_mainloop_api_init(&pulse->pa_mainloop, pulse->ev_base, 1, 1, 1);
  
  /* Create Pulseaudio context */
  
  pulse->pa_ctx = pa_context_new(&pulse->pa_mainloop, PROJECT);
  pa_context_set_state_callback(pulse->pa_ctx, _pulse_ctx_state, pulse);
  
  syslog(LOG_INFO, "Connecting Pulseaudio Context");
  
  pulse->pa_server = getenv("_PULSE");
  pa_context_connect(pulse->pa_ctx, pulse->pa_server, PA_CONTEXT_NOFLAGS/* | PA_CONTEXT_NOFAIL*/, NULL);
}

static void pulse_ctx_done(pulse_t* pulse){
  event_del(&pulse->ev_conn);
  
  if(pulse->pa_ctx){
    pulse_dev_done(pulse);
  }
}

static void _pulse_ctx_conn(evutil_socket_t fd, short ev, void *ud){
  pulse_t* pulse = ud;
  
  event_del(&pulse->ev_conn);
  
  pulse_ctx_init(pulse);
}

void pulse_init(pulse_t* pulse, struct event_base* ev_base){
  pulse->ev_base = ev_base;
  
  pulse->retry_delay.tv_sec = PULSE_RETRY_DELAY;
  pulse->retry_delay.tv_usec = 0;
  
  pulse->pa_server = NULL;
  pulse->pa_ctx = NULL;
  
  pulse->pa_state = PA_CONTEXT_UNCONNECTED;
  
  const char* modld = getenv("_MODLD");
  pulse->modld = modld && !strcmp(modld, "no") ? 0 : 1;
  
  pulse->pipe_source = NULL;
  pulse->pipe_sink = NULL;

  pulse->idx_source = 0;
  pulse->idx_sink = 0;
  
  pulse->fd_source = 0;
  pulse->fd_sink = 0;
  
  pulse->active = 0;
  
  pulse->io_delay.tv_sec = 0;
  pulse->io_delay.tv_usec = AUDIO_IO_TIMEOUT;
  
  event_assign(&pulse->ev_conn, pulse->ev_base, -1, EV_TIMEOUT, _pulse_ctx_conn, pulse);
  event_priority_set(&pulse->ev_conn, 0);
  
  pulse_ctx_init(pulse);
}

void pulse_done(pulse_t* pulse){
  pulse_ctx_done(pulse);
}

#endif
