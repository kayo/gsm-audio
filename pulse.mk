ifeq ($(pulse),on)

pulse.source.name?=$(project)_input
pulse.source.desc?=GSM Audio Input

pulse.sink.name?=$(project)_output
pulse.sink.desc?=GSM Audio Output

pulse.retry_delay?=5

pulse.opts=$(call hide-spaces,format=$(audio.format) rate=$(audio.rate) channels=$(audio.channels))

def+=PULSE_ENABLE=1

def+=PULSE_SOURCE_NAME='"$(pulse.source.name)"'
def+=PULSE_SOURCE_DESC='"$(call hide-spaces,$(pulse.source.desc))"'
def+=PULSE_SOURCE_OPTS='"$(pulse.opts)"'

def+=PULSE_SINK_NAME='"$(pulse.sink.name)"'
def+=PULSE_SINK_DESC='"$(call hide-spaces,$(pulse.sink.desc))"'
def+=PULSE_SINK_OPTS='"$(pulse.opts)"'

def+=PULSE_RETRY_DELAY=$(pulse.retry_delay)

pkg.deps+=libpulse

endif
