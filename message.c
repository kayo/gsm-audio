#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>

#include "message.h"

static const char *
message_helper_rule(message_helper *filter){
  static const char *item = ",%s='%s'";
  static char rule[256];
  char *pos = rule;
  
#define _(name, cond, value) if(cond){                                  \
    pos += snprintf(pos, rule + sizeof(rule) - pos,                     \
                    item + (pos == rule ? 1 : 0) /* skip first coma for first rule */, \
                    #name, value);                                      \
  }
  _(type, filter->type, filter->type == MESSAGE_METHOD ? "method_call" : "signal");
  _(path, filter->path, filter->path);
  _(interface, filter->iface, filter->iface);
  _(member, filter->name, filter->name);
  _(destination, filter->service, filter->service);
#undef _
  
  return rule;
}

static inline const char *dbus_message_get_typename(DBusMessage *message){
  switch(dbus_message_get_type(message)){
  case DBUS_MESSAGE_TYPE_METHOD_CALL: return "method_call";
  case DBUS_MESSAGE_TYPE_METHOD_RETURN: return "method_return";
  case DBUS_MESSAGE_TYPE_SIGNAL: return "signal";
  case DBUS_MESSAGE_TYPE_ERROR: return "error";
  }
  return "invalid";
}

static DBusHandlerResult
message_helper_handle(DBusConnection *connection,
                      DBusMessage *message,
                      void *userdata){
  
  message_helper *filter = (message_helper*)userdata;
  
  if(!filter->handler){
    return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
  }

  if(filter->service && !dbus_message_has_destination(message, filter->service)){
    return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
  }
  
  if(filter->path && !dbus_message_has_path(message, filter->path)){
    return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
  }
  
  if(filter->type == MESSAGE_METHOD && !dbus_message_is_method_call(message, filter->iface, filter->name)){
    return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
  }
  
  if(filter->type == MESSAGE_SIGNAL && !dbus_message_is_signal(message, filter->iface, filter->name)){
    return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
  }
  
#ifdef DEBUG
  syslog(LOG_DEBUG, "<incoming> sender='%s',destination='%s',path='%s',interface='%s',member='%s',type='%s',serial=%d", dbus_message_get_sender(message), dbus_message_get_destination(message), dbus_message_get_path(message), dbus_message_get_interface(message), dbus_message_get_member(message), dbus_message_get_typename(message), dbus_message_get_serial(message));
#endif

  if(!filter->handler(connection, message, filter->userdata)){
    return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
  }
  
  return DBUS_HANDLER_RESULT_HANDLED;
}

int message_helper_attach(message_helper *filter, DBusConnection *connection){
  DBusError error;
  dbus_error_init(&error);
  
  dbus_bus_add_match(connection, message_helper_rule(filter), &error);
  
  if(dbus_error_is_set(&error)){
    syslog(LOG_ERR, "Add match failed (%s)", error.message); 
    dbus_error_free(&error); 
    return EXIT_FAILURE;
  }
  
  dbus_connection_add_filter(connection, message_helper_handle, filter, NULL);
  
  return EXIT_SUCCESS;
}

int message_helper_detach(message_helper *filter, DBusConnection *connection){
  dbus_connection_remove_filter(connection, message_helper_handle, filter);
  
  DBusError error;
  dbus_error_init(&error);
  
  dbus_bus_remove_match(connection, message_helper_rule(filter), &error);
  
  if(dbus_error_is_set(&error)){
    syslog(LOG_ERR, "Remove match failed (%s)", error.message); 
    dbus_error_free(&error); 
    return EXIT_FAILURE;
  }
  
  return EXIT_SUCCESS;
}

DBusMessage *message_helper_create(message_helper *filter, const char *destination){
  if(filter->type == MESSAGE_METHOD){
    return dbus_message_new_method_call(destination, filter->path, filter->iface, filter->name);
  }
  if(filter->type == MESSAGE_SIGNAL){
    return dbus_message_new_signal(filter->path, filter->iface, filter->name);
  }
  return NULL;
}

DBusMessage *message_helper_return(message_helper *filter, DBusMessage *request){
  DBusMessage *response = NULL;
  
  if(filter->type == MESSAGE_METHOD){
    response = dbus_message_new_method_return(request);
  }
  
  //dbus_message_unref(request);
  
  return response;
}

int message_helper_output(DBusConnection *connection, DBusMessage *message){
  dbus_uint32_t serial = 0;
  
#ifdef DEBUG
  syslog(LOG_DEBUG, "<outgoing> sender='%s',destination='%s',path='%s',interface='%s',member='%s',type='%s',serial=%d", dbus_message_get_sender(message), dbus_message_get_destination(message), dbus_message_get_path(message), dbus_message_get_interface(message), dbus_message_get_member(message), dbus_message_get_typename(message), dbus_message_get_serial(message));
#endif
  
  if(!dbus_connection_send(connection, message, &serial)){
    syslog(LOG_ERR, "Unable to send message: Out of Memory");
    EXIT_FAILURE;
  }
  
  dbus_connection_flush(connection);
  
  //dbus_message_unref(message);
  
  return EXIT_SUCCESS;
}

char *message_helper_mkpath(const char* root, const char* node){
  size_t root_l = root ? strlen(root) : 0;
  size_t node_l = node ? strlen(node) : 0;
  
  char* path = calloc(root_l + 1 + node_l + 1, 1);
  
  if(root && root_l){
    if(root[root_l - 1] == '/'){
      root_l --;
    }
    strncpy(path, root, root_l);
  }
  
  path[root_l] = '/';
  
  if(node && node_l){
    strcpy(&path[root_l + 1], node);
  }else{
    path[root_l + 1] = '\0';
  }
  
  return path;
}

#define I11N_HEAD "<!DOCTYPE node PUBLIC \"-//freedesktop//DTD D-BUS Object Introspection 1.0//EN\"\n \"http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd\">\n<node>\n"
#define I11N_FOOT "</node>\n"

static char i11n_data[10 * 1024] = I11N_HEAD; // 10 Kbytes
#define I11N_INIT (i11n_data + sizeof(I11N_HEAD) - 1)
static char *i11n_last = I11N_INIT;
#define I11N_LAST ((i11n_data + sizeof(i11n_data)) - i11n_last)

#define message_helper_i11n_append(args...) {               \
    i11n_last += snprintf(i11n_last, I11N_LAST, ##args);    \
  }

const char *message_helper_i11n(){
  message_helper_i11n_append(I11N_FOOT);
  i11n_last = I11N_INIT;
  return i11n_data;
}

void message_helper_i11n_iface(const char *name){
  if(name){
    message_helper_i11n_append("  <interface name=\"%s\">\n", name);
  }else{
    message_helper_i11n_append("  </interface>\n");
  }
}

void message_helper_i11n_method(const char *name){
  if(name){
    message_helper_i11n_append("    <method name=\"%s\">\n", name);
  }else{
    message_helper_i11n_append("    </method>\n");
  }
}

void message_helper_i11n_signal(const char *name){
  if(name){
    message_helper_i11n_append("    <signal name=\"%s\">\n", name);
  }else{
    message_helper_i11n_append("    </signal>\n");
  }
}

void message_helper_i11n_arg(const char *name, const char *type, message_direction direction){
  message_helper_i11n_append("      <arg name=\"%s\" type=\"%s\"%s/>\n", name, type,
                             direction == MESSAGE_NONE ? "" : direction == MESSAGE_INPUT ?
                             " direction=\"in\"" : " direction=\"out\"");
}

void message_helper_i11n_node(const char *name){
  message_helper_i11n_append("  <node name=\"%s\"/>\n", name);
}
