#include "common.h"
#include "service.h"

#include <string.h>

int main(){
  const char* log = getenv("_SYSLOG");
  
  if(!log || strcmp(log, "only") != 0){
    openlog(PROJECT, LOG_CONS | LOG_PERROR, LOG_DAEMON);
  }
  
  service_t main;
  
  service_init(&main);

  service_loop(&main);

  service_done(&main);
  
  return 0;
}
