audio.format?=S16LE
audio.rate?=8000
audio.channels?=1

audio.samples?=160
audio.timeout?=20000

audio.error_limit?=6
audio.retry_delay?=5

def+=AUDIO_FORMAT=$(audio.format)
def+=AUDIO_RATE=$(audio.rate)
def+=AUDIO_CHANNELS=$(audio.channels)

def+=AUDIO_ERROR_LIMIT=$(audio.error_limit)
def+=AUDIO_RETRY_DELAY=$(audio.retry_delay)

def+=AUDIO_IO_SAMPLES=$(audio.samples)
def+=AUDIO_IO_TIMEOUT=$(audio.timeout)
