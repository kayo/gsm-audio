#ifndef __sound_h__
#define __sound_h__ "sound.h"

#include <stddef.h>
#include <stdint.h>

typedef int16_t sample_t;
typedef int32_t sample_double_t;
typedef uint16_t sample_module_t;

#define sample_max 32767
#define sample_min -sample_max
#define samples_from_bytes(bytes) ((bytes) >> 1)
#define samples_to_bytes(samples) ((samples) << 1)

typedef struct sound_ sount_t;

struct sound_ {
  size_t rate;
  float period;
};

typedef struct sample_mixer_ sample_mixer_t;

struct sample_mixer_ {
  sample_double_t pow;
  size_t count;
};

void sample_mixer_init(sample_mixer_t *mixer, size_t count);
sample_t sample_mixer_step(sample_mixer_t *mixer, sample_t sample, ...);
void sample_mixer_loop(sample_mixer_t *mixer, size_t samples, sample_t *output, const sample_t *input, ...);

#endif//__sound_h__
