#ifdef ALSA_ENABLE

#include "common.h"
#include "alsad.h"

static void _alsad_stream_done(alsad_stream_t* stm){
  if(stm->pcm){
    syslog(LOG_INFO, "Close %s alsa device (%s)", stm->dir == SND_PCM_STREAM_CAPTURE ? "capture" : "playback", stm->dev);
    
    snd_pcm_close(stm->pcm);
    stm->pcm = NULL;
  }
}

static int _alsad_stream_init(alsad_stream_t *stm){
  int err;

  //snd_pcm_hw_params_t *hw_params = NULL;
  //snd_pcm_sw_params_t *sw_params = NULL;
  
  stm->pcm = NULL;
  
  syslog(LOG_INFO, "Opening %s alsa device (%s)", stm->dir == SND_PCM_STREAM_CAPTURE ? "capture" : "playback", stm->dev);
  
  if((err = snd_pcm_open(&stm->pcm, stm->dev, stm->dir, SND_PCM_NONBLOCK)) < 0){
    syslog(LOG_ERR, "Cannot open audio device (%s)", snd_strerror(err));
    goto done;
  }
  
  /* setup hardware */
  /*
  if((err = snd_pcm_hw_params_malloc(&hw_params)) < 0){
    syslog(LOG_ERR, "Cannot allocate hardware parameter structure (%s)", snd_strerror(err));
    goto done;
  }
  
  if((err = snd_pcm_hw_params_any(stm->pcm, hw_params)) < 0){
    syslog(LOG_ERR, "Cannot initialize hardware parameter structure (%s)", snd_strerror(err));
    goto done;
  }
	
  if((err = snd_pcm_hw_params_set_access(stm->pcm, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0){
    syslog(LOG_ERR, "Cannot set access type (%s)", snd_strerror(err));
    goto done;
  }
	
  if((err = snd_pcm_hw_params_set_format(stm->pcm, hw_params, SND_PCM_FORMAT_S16_LE)) < 0){
    syslog(LOG_ERR, "Cannot set sample format (%s)", snd_strerror(err));
    goto done;
  }
	
  if((err = snd_pcm_hw_params_set_rate(stm->pcm, hw_params, 8000, 0)) < 0){
    syslog(LOG_ERR, "Cannot set sample rate (%s)", snd_strerror(err));
    goto done;
  }
	
  if((err = snd_pcm_hw_params_set_channels (stm->pcm, hw_params, 1)) < 0){
    syslog(LOG_ERR, "Cannot set channel count (%s)", snd_strerror(err));
    goto done;
  }
	
  if((err = snd_pcm_hw_params(stm->pcm, hw_params)) < 0){
    syslog(LOG_ERR, "Cannot set hw parameters (%s)", snd_strerror(err));
    goto done;
  }
  */
  
  /* setup software */

  
  
  syslog(LOG_INFO, "Set parameters (channels:%d, rate:%d, latency:%d)", AUDIO_CHANNELS, AUDIO_RATE, stm->lat);
  
  if((err = snd_pcm_set_params(stm->pcm,
                               SND_PCM_FORMAT_S16_LE,
                               SND_PCM_ACCESS_RW_INTERLEAVED,
                               AUDIO_CHANNELS, AUDIO_RATE, 0, stm->lat)) < 0){
    syslog(LOG_ERR, "Cannot set parameters (%s)", snd_strerror(err));
    goto done;
  }
  
  syslog(LOG_INFO, "Alsa device (%s) opened", stm->dev);
  
 done:
  /*
  if(hw_params){
    snd_pcm_hw_params_free(hw_params);
  }
  
  if(sw_params){
    snd_pcm_sw_params_free(sw_params);
  }
  */
  
  if(err < 0){
    _alsad_stream_done(stm);
  }
  
  return err;
}

static char* _alsad_stream_device(json_object* val, const char *opt_var, const char *env_var){
  char *name = NULL;
  
  if(val && json_object_is_type(val, json_type_object)){
    val = json_object_object_get(val, opt_var);
    
    if(val && json_object_is_type(val, json_type_object)){
      val = json_object_object_get(val, "device");
      
      if(val && json_object_is_type(val, json_type_string)){
        name = strdup(json_object_get_string(val));
      }
    }
  }
  
  if(!name){
    name = getenv(env_var);
    
    if(name){
      name = strdup(name);
    }
  }
  
  return name;
}

static uint32_t _alsad_stream_latency(json_object* val, const char *opt_var, const char *env_var){
  uint32_t latency = 0;
  
  if(val && json_object_is_type(val, json_type_object)){
    val = json_object_object_get(val, opt_var);
    
    if(val && json_object_is_type(val, json_type_object)){
      val = json_object_object_get(val, "latency");
      
      if(val && json_object_is_type(val, json_type_int)){
        latency = json_object_get_int(val);
      }
    }
  }
  
  if(!latency){
    const char *str = getenv(env_var);
    
    if(str){
      latency = strtoul(str, NULL, 10);
    }
  }
  
  if(!latency){
    latency = ALSA_LATENCY;
  }
  
  return latency;
}

void alsad_init(alsad_t* alsad, struct event_base* ev_base, json_object* opts){
  alsad->capture.dir = SND_PCM_STREAM_CAPTURE;
  alsad->playback.dir = SND_PCM_STREAM_PLAYBACK;
  
  if(opts && json_object_is_type(opts, json_type_object)){
    alsad->capture.dev = _alsad_stream_device(opts, "capture", "ALSA_CAPTURE_DEVICE");
    alsad->playback.dev = _alsad_stream_device(opts, "playback", "ALSA_PLAYBACK_DEVICE");
    
    alsad->capture.lat = _alsad_stream_latency(opts, "capture", "ALSA_CAPTURE_LATENCY");
    alsad->playback.lat = _alsad_stream_latency(opts, "playback", "ALSA_PLAYBACK_LATENCY");
  }
  
  _alsad_stream_init(&alsad->capture);
  _alsad_stream_init(&alsad->playback);
}

void alsad_done(alsad_t* alsad){
  _alsad_stream_done(&alsad->capture);
  _alsad_stream_done(&alsad->playback);
  
  if(alsad->capture.dev){
    free(alsad->capture.dev);
  }
  
  if(alsad->playback.dev){
    free(alsad->playback.dev);
  }
}

static int _alsad_stream_toggle(alsad_stream_t* stm, char active){
  if(!stm->pcm){
    return 0;
  }
  
  int err;
  
  if(active){ /* start */
    if((err = snd_pcm_prepare(stm->pcm)) < 0){
      syslog(LOG_ERR, "Cannot prepare audio interface for use (%s)", snd_strerror(err));
      return err;
    }
    //if(stm->dir == SND_PCM_STREAM_PLAYBACK){
    /* we need to seek the stream to reduce latency */
    if((err = snd_pcm_reset(stm->pcm)) < 0){
      syslog(LOG_ERR, "Cannot reset audio interface (%s)", snd_strerror(err));
      return err;
    }
    //}
    if(stm->dir == SND_PCM_STREAM_CAPTURE){
      /* we must to start capture stream */
      if((err = snd_pcm_start(stm->pcm)) < 0){
        syslog(LOG_ERR, "Cannot start audio interface (%s)", snd_strerror(err));
        return err;
      }
    }
  }else{ /* stop */
    /* we need to drop all uncaptured/unplayed data */
    if((err = snd_pcm_drop(stm->pcm)) < 0){
      syslog(LOG_ERR, "Cannot drop audio interface (%s)", snd_strerror(err));
      return err;
    }
  }
  
  return 0;
}

static snd_pcm_sframes_t _alsad_stream_poll(alsad_stream_t* stm){
  if(!stm->pcm){
    return 0;
  }
  
  snd_pcm_sframes_t frames;
  
  if((frames = snd_pcm_avail_update(stm->pcm)) < 0){
    if(frames == -EPIPE){
      syslog(LOG_WARNING, stm->dir == SND_PCM_STREAM_PLAYBACK ? "An underrun occured" : "An overrun occured");
    }else{
      syslog(LOG_ERR, "Unknown ALSA avail update return value (%d)", frames);
    }
    snd_pcm_recover(stm->pcm, frames, 1);
  }
  
  return frames;
}

void alsad_io(alsad_t* alsad, size_t samples, const sample_t* captured, sample_t* playback){
  char active = samples > 0 ? 1 : 0;
  
  if(active != alsad->active){ /* toggle activity */
    _alsad_stream_toggle(&alsad->capture, active);
    _alsad_stream_toggle(&alsad->playback, active);
    
    alsad->active = active;
  }
  
  snd_pcm_sframes_t frames, result;
  
  if((frames = _alsad_stream_poll(&alsad->playback)) >= samples){
    if((result = snd_pcm_writei(alsad->playback.pcm, captured, samples)) < 0){
      syslog(LOG_ERR, "Write audio failed (%s)", snd_strerror(result));
    }
  }

  LOG("Alsa playback:%d samples:%d, frames:%d", snd_pcm_state(alsad->playback.pcm), samples, frames);
  
  if((frames = _alsad_stream_poll(&alsad->capture)) >= samples){
    if((result = snd_pcm_readi(alsad->capture.pcm, playback, samples)) < 0){
      syslog(LOG_ERR, "Read audio failed (%s)", snd_strerror(result));
    }
  }
  
  LOG("Alsa capture:%d samples:%d, frames:%d", snd_pcm_state(alsad->capture.pcm), samples, frames);
}

#endif//ALSA_ENABLE
