#ifdef PULSE_ENABLE

#ifndef __pulse_h__
#define __pulse_h__ "pulse.h"

#include "sound.h"

#include <pulse/pulseaudio.h>

typedef struct pulse_ pulse_t;

struct pulse_ {
  /* Common section */
  
  struct event_base *ev_base;
  
  /* PulseAudio section */
  
  const char* pa_server;
  pa_context* pa_ctx;
  int pa_recon;
  int pa_state;
  
  struct timeval retry_delay;
  struct event ev_conn;
  
  char modld;
  
  const char* pipe_source;
  const char* pipe_sink;
  
  uint32_t idx_source;
  uint32_t idx_sink;
  
  int fd_source;
  int fd_sink;
  
  char active;
  struct event ev_data;
  sample_t data_buf[AUDIO_IO_SAMPLES];
  size_t data_len;
  struct timeval io_delay;
  
  pa_mainloop_api pa_mainloop;
};

void pulse_init(pulse_t* pulse, struct event_base* ev_base);
void pulse_done(pulse_t* pulse);

void pulse_io(pulse_t* pulse, size_t samples, const sample_t* captured, sample_t* playback);

#endif//__pulse_h__

#endif//PULSE_ENABLE
