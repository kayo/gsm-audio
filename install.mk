bin.dir?=/usr/sbin
dbus.conf.dir?=/etc/dbus-1/system.d
systemd.conf.dir?=/etc/systemd/system

install:
	systemctl stop $(project) || true
	cp $(project).elf $(bin.dir)/$(project)
	cp $(project).conf $(dbus.conf.dir)
	systemctl reload dbus
	cp $(project).service $(systemd.conf.dir)
	systemctl --system daemon-reload
	systemctl enable $(project)
	systemctl start $(project)

uninstall:
	systemctl stop $(project)
	systemctl disable $(project)
	rm -f $(bin.dir)/$(project)
	rm -f $(dbus.conf.dir)/$(project).conf
	rm -f $(system.service.dir)/$(project).service
	systemctl --system daemon-reload
